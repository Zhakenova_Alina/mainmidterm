package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.PersonRepository;
import kz.aitu.advancedJava.service.PersonService;
import kz.aitu.advancedJava.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class PersonController {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private PersonService personService;

    public PersonController(PersonService personService) {

        this.personService = personService;
    }

    @GetMapping("/api/v2/users")
    public String showTables(Model model) {

        Iterable<Person> a = personRepository.findAll();
        model.addAttribute("a", a);
        return "index";
    }

    @PutMapping("/api/v2/users/")

    public ResponseEntity<?> updatePerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.save(person));
    }


    @PostMapping("/api/v2/users/")
    public ResponseEntity<?> createPerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.create(person));
    }


    @DeleteMapping("/api/v2/users/{id}")
    public void deletePerson(@PathVariable long id) {
        personService.delete(id);
    }
}
