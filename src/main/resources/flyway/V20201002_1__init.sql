DROP TABLE IF EXISTS Person;
create table Person (
	id serial not null,
	firstname varchar default 128,
	lastname varchar default 128,
	city varchar default 255,
	phone INT NOT null ,
	telegram varchar default 255

);


ALTER TABLE Person owner to postgres;